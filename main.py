import schedule
from flask import Flask, render_template
from flask_socketio import SocketIO

APP = Flask('ChatBot')
APP.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'
APP.config['STATIC_FOLDER'] = 'static'
socketio = SocketIO(APP)


@APP.route('/', methods=['GET'])
def sessions():
    return render_template('session.html')


def message_received(methods=['GET', 'POST']):
    print('message was received!!!')


def send_message(message):
    print('sending message')
    socketio.emit('Response', message)


@socketio.on('message')
def handle_event(json, methods=['GET', 'POST']):
    print('Received event %s ' % str(json))
    socketio.emit('Response', json, callback=message_received)


if __name__ == '__main__':
    print('starting socket io')
    socketio.run(APP, debug=True)

schedule.every(5).seconds.do(send_message('hi'))
